package com.study.springboot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MyController {
	private static final Logger logger = LoggerFactory.getLogger(MyController.class);
	

    @RequestMapping("/")
    public  String root() throws Exception{
        return "createPage";
    }
 
    @RequestMapping("/create")
    public String test1(Member member, Model model)
    {
    	logger.info(member.toString());
    	System.out.println(member);
    	System.out.println(member.name.length());
        return "createPage";       
    }

}
